class Step {
    name
    fn

    constructor(name, fn) {
        this.name = name
        this.fn = fn
    }

    execute() {
        return this.fn()
    }
}

module.exports = Step